package com.example.demo.bean;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;

public class Test {
    public static void main(String[] args) throws NoSuchFieldException, NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        User user = new User();


        String key = "username";

        String k = getMethodName(key);

        System.out.println("k: " + k);

        Class userClazz = user.getClass();
        Field declaredField = userClazz.getDeclaredField(key);

        Class<?> type = declaredField.getType();
        System.out.println(type);

        userClazz.getMethod(k, type).invoke(user, "测试修改姓名");
        System.out.println(user);

    }

    public static String getMethodName(String str) {
        return "set" + str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
