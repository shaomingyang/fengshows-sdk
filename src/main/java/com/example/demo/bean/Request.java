package com.example.demo.bean;

import lombok.Data;

@Data
public class Request {

    private String id;

    private String method;

    private Object params;

}
