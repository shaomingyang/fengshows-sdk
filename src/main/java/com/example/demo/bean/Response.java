package com.example.demo.bean;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;
import org.jetbrains.annotations.NotNull;
import org.springframework.http.HttpStatus;

@Data
@ToString
@Builder
public class Response {

    private String id;

    private int status;

    private Object data;

    private String msg;

    public static Response success(@NotNull String id, @NotNull Object data) {
        return Response.builder().id(id).data(data).status(HttpStatus.OK.value()).msg("ok").build();
    }

    public static Response fail(@NotNull String id, @NotNull Exception e) {
        return Response.builder().id(id).status(HttpStatus.INTERNAL_SERVER_ERROR.value()).msg(e.getMessage()).build();
    }

    public static Response fail(@NotNull String id, @NotNull MyException e) {
        return Response.builder().id(id).status(e.getStatus()).msg(e.getMsg()).build();
    }
}
