package com.example.demo.bean;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class MyException extends RuntimeException {
    private static final long serialVersionUID = 2011421635796554875L;

    private int status;

    private String msg;

}
