package com.example.demo.sdk.server;

import java.util.Optional;

public class WebviewHandler {
    private static ThreadLocal<String> threadLocal = new ThreadLocal<>();

    public static String get() {
        return Optional.of(threadLocal.get()).get();
    }

    public static void set(String Webview) {
        threadLocal.set(Webview);
    }

    public static void remove() {
        threadLocal.remove();
    }
}
