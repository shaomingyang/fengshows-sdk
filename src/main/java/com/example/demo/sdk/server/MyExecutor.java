package com.example.demo.sdk.server;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class MyExecutor {
    private static final ExecutorService threadPool = Executors.newCachedThreadPool();

    private MyExecutor() {
    }

    public static ExecutorService getThreadPool() {
        return threadPool;
    }

}
