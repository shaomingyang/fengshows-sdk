package com.example.demo.sdk.server;

import com.example.demo.bean.Request;
import com.example.demo.bean.Response;
import com.example.demo.sdk.FengshowsSDK;
import com.example.demo.sdk.SDK;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@Slf4j
public class Worker {
    private static final ExecutorService cachedThreadPool = MyExecutor.getThreadPool();
    private static final FengshowsSDK sdk = new SDK();


    public static Response work(Request request, String Webview) throws ExecutionException, InterruptedException {
        // getRequest
        Future<Response> future = cachedThreadPool.submit(new Task(request, Webview));

        return future.get();
    }

    @AllArgsConstructor
    private static class Task implements Callable<Response> {
        private Request request;
        private String webView;

        @Override
        public Response call() {
            Response response;
            try {
                WebviewHandler.set(webView);
                response = (Response) sdk.processRequest(request);
            } catch (Exception e) {
                e.printStackTrace();
                log.info("测试调用方法");
                throw new RuntimeException(e);
            } finally {
                WebviewHandler.remove();
            }
            return response;
        }
    }
}
