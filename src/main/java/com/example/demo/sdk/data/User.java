package com.example.demo.sdk.data;

import com.example.demo.sdk.server.MyExecutor;
import com.example.demo.sdk.server.WebviewHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;

@Slf4j
public class User {
    private final ExecutorService threadPool = MyExecutor.getThreadPool();

    public com.example.demo.bean.User get(Object jobParam) throws ExecutionException, InterruptedException {

        final String webview = WebviewHandler.get();

        Future<com.example.demo.bean.User> future = threadPool.submit(() -> {
            com.example.demo.bean.User user;
            try {
                Thread.sleep(10000);
                log.info("request = {},webview = {}", jobParam, webview);
                user = new com.example.demo.bean.User("小明");



            } catch (Exception e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            return user;
        });

        log.info("异步任务");
        return future.get();
    }
}
