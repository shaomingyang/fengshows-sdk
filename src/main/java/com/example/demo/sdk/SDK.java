package com.example.demo.sdk;

import com.alibaba.fastjson.JSON;
import com.example.demo.bean.Request;
import com.example.demo.bean.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.support.PropertiesLoaderUtils;

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.*;

@Slf4j
public class SDK implements FengshowsSDK {
    private static final HashMap<String, Object> map = new HashMap<>();

    // 读取配置文件,并注册
    static {
        String fileName = "fengshows.properties";
        try {
            Properties properties = PropertiesLoaderUtils.loadAllProperties(fileName);
            for (Map.Entry<Object, Object> entry : properties.entrySet()) {
                String key = entry.getKey().toString();
                String value = entry.getValue().toString();
                log.debug("加载bean配置,key={},value={}", key, value);
                map.put(key, Class.forName(value).newInstance());
            }
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public Object processRequest(Object obj) {
//        JSONObject requestJSON = JSONObject.parseObject(obj);
//        Request req = JSON.toJavaObject(requestJSON,Request.class);

        Request req = (Request) obj;
        Response res;
        try {
            String method = req.getMethod();
            log.debug(method);
            String[] str = method.split("\\.");
            log.debug(Arrays.toString(str));
            Object object = Optional.ofNullable(map.get(str[0])).get();

//          Arrays.asList(object.getClass().getMethods()).stream().forEach(System.out::println);

            Method mt = object.getClass().getMethod(str[1], Object.class);
            Object data = mt.invoke(object, req.getParams());
            res = Response.success(req.getId(), data);
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();

            res = Response.fail(req.getId(), e);
        }

        String s = JSON.toJSONString(res);
        log.info(s);
        return res;
    }
}


