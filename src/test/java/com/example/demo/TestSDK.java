package com.example.demo;

import com.example.demo.bean.JobParam;
import com.example.demo.bean.Request;
import com.example.demo.sdk.SDK;
import com.example.demo.sdk.server.Worker;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutionException;


@RunWith(SpringRunner.class)
@SpringBootTest
public class TestSDK {


    @Test
    public void testSDK() {
        SDK sdk = new SDK();
        Request req = new Request();
        req.setMethod("user.get");
        req.setId("jsb_1");
        req.setParams(new JobParam("test"));
        Object res = sdk.processRequest(req);
        System.out.println(res);
    }

    @Test
    public void testWorker() {
        Request req = new Request();
        req.setMethod("user.get");
        req.setId("jsb_2");
        req.setParams(new JobParam("test2"));
        try {
            Object obj = Worker.work(req, "hello");
            System.out.println(obj);
        } catch (ExecutionException | InterruptedException e) {
            e.printStackTrace();
        }

    }

}
